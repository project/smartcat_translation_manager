# INTRODUCTION

**Smartcat** connects linguists, companies, and agencies to streamline the
translation of any content into any language, on demand. Our platform puts
your translation process on autopilot, from content creation to payments. 
The extension works by linking your Drupal website to a Smartcat account 
and pushing the content for translation upon request. After the translation
is done in Smartcat — by yourself, your own volunteers, or hired vendors — 
it is automatically pulled back into Drupal.

## Account & pricing

You need to create a Smartcat account as one is not automatically created 
when installing the extension. To create an account, please visit the signup
page at 
[www.smartcat.ai](https://www.smartcat.ai/?utm_source=connectors&utm_medium=referral&utm_campaign=drupal).
All translation features in Smartcat are free to use.

## Features

*   Integrate your Drupal website with a specific Smartcat account
*   Create multiple translation profiles with different settings
*   Choose the translation vendor
*   Automatically send new or updated content for translation
*   Choose translation workflow stages —
    translation, editing, proofreading, etc.
*   Send content to a specific Smartcat project
*   Send content for translation in batches

## Benefits of Smartcat

*   No document re-formatting required
*   Easy-to-use multilingual translation editor
*   Multi-stage translation process — e.g., translation, editing, proofreading
*   Free collaboration with your own volunteers or coworkers
*   Marketplace with 250,000+ translators and 2,000+ agencies 
    in 100+ language pairs
*   Track progress by language, document, or person
*   Automated payments to translation suppliers
*   Free support to optimize localization processes

# REQUIREMENTS

*   Drupal 8.*
*   Enabled Content Translation Module
*   PHP 7

# INSTALLATION

Just usually drupal module installation.
After installation submit contact form, please.

# CONFIGURATION

Need configure conection to smartcat.
Select Server, enter "Smartcat account ID" and "API key".
