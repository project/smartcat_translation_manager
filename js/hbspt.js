/** form for info */

hbspt.forms.create({
  portalId: '4950983',
  formId: '5da21dc3-49d9-49bb-aec1-2272338dbdcb',
  onFormReady: function ($form) {
    'use strict';
    $form.find('input[name="utm_source"]').val('connectors');
    $form.find('input[name="utm_medium"]').val('referral');
    $form.find('input[name="utm_campaign"]').val('drupal');
  },
  onFormSubmit: function () {
    'use strict';
    hbsptDialog.close();
  }
});
const form = jQuery('.hbspt-form');
const hbsptDialog = Drupal.dialog(form[0], {
  resizable: true,
  height: 560,
  width: 400,
  modal: true,
  title: Drupal.t('Send your contact'),
  close: function (event, ui) {
    'use strict';
    addOrUpdateUrlParam('hbspt_form', 1);
  }
});

hbsptDialog.show();

function addOrUpdateUrlParam(name, value) {
  'use strict';
  const href = window.location.href;
  let regex = new RegExp('[&\\?]' + name + '=');
  if (regex.test(href)) {
    regex = new RegExp('([&\\?])' + name + '=\\d+');
    window.location.href = href.replace(regex, '$1' + name + '=' + value);
  }
  else {
    if (href.indexOf('?') > -1) {
      window.location.href = href + '&' + name + '=' + value;
    }
    else {
      window.location.href = href + '?' + name + '=' + value;
    }
  }
}
