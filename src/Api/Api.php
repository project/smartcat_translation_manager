<?php

namespace Drupal\smartcat_translation_manager\Api;

use SmartCat\Client\SmartCat;
use Drupal\Core\State\StateInterface;
use Drupal\smartcat_translation_manager\DB\Entity\Project as ProjectEntity;

/**
 * Facade class for work with Smartcat API .
 */
class Api {
  /**
   * Client smartcat API.
   *
   * @var \SmartCat\Client\SmartCat
   */
  protected $api;

  /**
   * Facade for directory API.
   *
   * @var Directory
   */
  protected $directory;

  /**
   * Facade for project API.
   *
   * @var \Drupal\smartcat_translation_manager\Api\Project
   */
  public $project;

  /**
   * Init API connection .
   */
  public function __construct(StateInterface $state) {

    $login = $state->get('smartcat_api_login');
    $passwd = $state->get('smartcat_api_password');
    $server = $state->get('smartcat_api_server');

    $this->api = new SmartCat($login, $passwd, $server);
    $this->directory = new Directory($this->api);
    $this->project = new Project($this->api);
  }

  /**
   * Method getting account info.
   *
   * @return \Psr\Http\Message\ResponseInterface|\SmartCat\Client\Model\AccountModel
   *   Return account model
   */
  public function getAccount() {
    return $this->api->getAccountManager()->accountGetAccountInfo();
  }

  /**
   * Get smartcat languages.
   *
   * @return \SmartCat\Client\Model\DirectoryItemModel[]
   *   Array directory item model
   */
  public function getLanguages() {
    return $this->directory->get('language')->getItems();
  }

  /**
   * Get service type.
   *
   * @return array
   *   Service types
   */
  public function getServiceTypes() {
    return $this->directory->getItemsAsArray('lspServiceType');
  }

  /**
   * Get vendor.
   *
   * @return array
   *   Vendors list
   */
  public function getVendor() {
    return $this->directory->getItemsAsArray('vendor');
  }

  /**
   * Get project statuses.
   *
   * @return array
   *   All project statuses
   */
  public function getProjectStatus() {
    return $this->directory->getItemsAsArray('projectStatus');
  }

  /**
   * Get document.
   *
   * @return \SmartCat\Client\Model\DocumentModel
   *   Document model
   */
  public function getDocument($externalDocumentId) {
    return $this->api->getDocumentManager()->documentGet(['documentId' => $externalDocumentId]);
  }

  /**
   * Get project.
   *
   * @return \SmartCat\Client\Model\ProjectModel
   *   Project model
   */
  public function getProject($externalProjectId) {
    return $this->api->getProjectManager()->projectGet($externalProjectId);
  }

  /**
   * Buile statistic.
   *
   * @return \SmartCat\Client\Model\ProjectModel
   *   Project model
   */
  public function buildStatistic($externalProjectId) {
    $scProject = $this->getProject($externalProjectId);

    $disasemblingSuccess = TRUE;
    foreach ($scProject->getDocuments() as $document) {
      if ($document->getDocumentDisassemblingStatus() != 'success') {
        $disasemblingSuccess = FALSE;
        break;
      }
    }

    if ($disasemblingSuccess) {
      $this->api->getProjectManager()->projectBuildStatistics($scProject->getId());
    }

    return $scProject;
  }

  /**
   * Request for export documents.
   *
   * @param array $documentIds
   *   List documents ids.
   *
   * @return \Psr\Http\Message\ResponseInterface|\SmartCat\Client\Model\ExportDocumentTaskModel
   *   Result request
   */
  public function requestExportDocuments(array $documentIds) {
    if (is_scalar($documentIds)) {
      $documentIds = [$documentIds];
    }
    return $this->api
      ->getDocumentExportManager()
      ->documentExportRequestExport(['documentIds' => $documentIds]);
  }

  /**
   * Download export documents.
   *
   * @param string $exportId
   *   Export Id.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Response with documents
   */
  public function downloadExportDocuments($exportId) {
    return $this->api
      ->getDocumentExportManager()
      ->documentExportDownloadExportResult($exportId);
  }

  /**
   * Create projects.
   *
   * @param \Drupal\smartcat_translation_manager\DB\Entity\Project $project
   *   Entity project.
   *
   * @return \Psr\Http\Message\ResponseInterface|\SmartCat\Client\Model\ProjectModel
   *   Project model
   */
  public function createProject(ProjectEntity $project) {
    $scNewProject = $this->project->createProject($project);
    return $this->api
      ->getProjectManager()
      ->projectCreateProject($scNewProject);
  }

  /**
   * Proxy for API SDK methods.
   *
   * @return mixed
   *   Result called method
   */
  public function __call($method, $arguments) {
    return $this->api->$method(...$arguments);
  }

}
