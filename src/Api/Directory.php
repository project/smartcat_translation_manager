<?php

namespace Drupal\smartcat_translation_manager\Api;

/**
 * Part facade API  for work with derectory resource.
 */
class Directory extends ApiBaseAbstract {
  protected $directories = [];

  /**
   * Get any directory.
   *
   * @param string $type
   *   Type directory.
   *
   * @return \SmartCat\Client\Model\DirectoryModel
   *   Return directory model
   */
  public function get(string $type) {
    if (!array_key_exists($type, $this->directories)) {
      $this->directories[$type] = $this->api
        ->getDirectoriesManager()
        ->directoriesGet(['type' => $type]);
    }
    return $this->directories[$type];
  }

  /**
   * Get any directory as array id=>name.
   *
   * @param string $type
   *   Type directory.
   *
   * @return array
   *   Array values
   */
  public function getItemsAsArray(string $type) {
    $array = [];
    $items = $this->get($type)->getItems();
    foreach ($items as $item) {
      $array[$item->getId()] = $item->getName();
    }
    return $array;
  }

}
