<?php

namespace Drupal\smartcat_translation_manager\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\smartcat_translation_manager\DB\Entity\Document;
use Drupal\smartcat_translation_manager\Helper\ApiHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for work with documents.
 */
class DocumentController extends ControllerBase {
  /**
   * Api service.
   *
   * @var \Drupal\smartcat_translation_manager\Api\Api
   */
  protected $api;

  /**
   * Document repository.
   *
   * @var \Drupal\smartcat_translation_manager\DB\Entity\DocumentRepository
   */
  protected $documentRepository;

  /**
   * Entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityManager;

  /**
   * Messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * Current HTTP request.
   *
   * @var Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Init dependencies.
   */
  public function __construct(ContainerInterface $container) {
    $this->api = $container->get('smartcat_translation_manager.api');
    $this->documentRepository = $container->get('smartcat_translation_manager.repository.document');
    $this->messenger = $container->get('messenger');
    $this->request = $container->get('request_stack')->getCurrentRequest();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container);
  }

  /**
   * Method for display list documents(dashboard)
   *
   * @return array
   *   Return renderable array.
   */
  public function content() {
    $table = [
      '#type' => 'table',
      '#title' => 'Dashboard',
      '#header' => [
        'Item',
        'Source language',
        'Target language',
        'Status',
        'Smartcat project',
      ],
      '#rows' => [],
    ];
    $criteria = [];
    $sendButtonKey = 'smartcat';
    try {
      $this->api->getAccount();
    }
    catch (\Exception $e) {
      $sendButtonKey = 'smartcat-disabled';
      $this->messenger->addError($this->t('Invalid Smartcat account ID or API key. Please check <a href=":url">your credentials</a>.', [
        ':url' => Url::fromRoute('smartcat_translation_manager.settings')->toString(),
      ], ['context' => 'smartcat_translation_manager']));
    }
    $document_id = $this->request->query->get('document_id');
    if ($document_id) {
      $criteria['id'] = $document_id;
    }

    $total = $this->documentRepository->count();
    $page = pager_find_page();
    $perPage = 10;
    $offset = $perPage * $page;
    pager_default_initialize($total, $perPage);

    $documents = $this->documentRepository->getBy($criteria, (int) $offset, $perPage, ['id' => 'DESC']);

    if (!empty($documents)) {
      foreach ($documents as $i => $document) {
        $operations = [
          'data' => [
            '#type' => 'operations',
            '#links' => [],
          ],
        ];

        $language = $this->languageManager()->getLanguage(strtolower($document->getSourceLanguage()));
        $targetLanguage = $this->languageManager()->getLanguage(strtolower($document->getTargetLanguage()));
        $options = ['language' => $language];
        $entity = $this->entityTypeManager()
          ->getStorage($document->getEntityTypeId())
          ->load($document->getEntityId());

        if ($entity) {

          if ($document->getStatus() === Document::STATUS_DOWNLOADED) {
            $newestDocs = $this->documentRepository
              ->getBy([
                'entityId' => $document->getEntityId(),
                'id' => [$document->getId(), '>'],
                'targetLanguage' => $document->getTargetLanguage(),
              ]);
            if (count($newestDocs) === 0) {
              $operations['data']['#links']['smartcat_refresh_doc'] = [
                'url' => Url::fromRoute(
                  'smartcat_translation_manager.document.refresh',
                  [],
                  [
                    'query' => [
                      'destination' => $this->request->getRequestUri(),
                      'document_id' => $document->getId(),
                    ],
                  ]
                ),
                'title' => $this->t('Check updates'),
              ];

              $query = [
                'entity_id' => $entity->id(),
                'type_id' => $entity->getEntityTypeId(),
                'lang' => $document->getTargetLanguage(),
              ];
              $url = Url::fromRoute('smartcat_translation_manager.project.add');
              $url->setOption('query', $query);
              $operations['data']['#links'][$sendButtonKey] = [
                'title' => $this->t('Send to Smartcat'),
                'url' => $url,
              ];
            }
          }

          $operations['data']['#links']['smartcat_doc'] = [
            'url' => ApiHelper::getDocumentUrl($document),
            'title' => $this->t('Go to Smartcat'),
          ];

          $edit_url = $entity->toUrl('canonical', $options);
          $table['#rows'][$i] = [
            Link::fromTextAndUrl($entity->label(), $edit_url),
            $language ? $language->getName() : $document->getSourceLanguage(),
            $targetLanguage ? $targetLanguage->getName() : $document->getTargetLanguage(),
            Document::STATUSES[$document->getStatus()],
            $operations,
          ];
        }
        else {
          $table['#rows'][$i] = [
            $this->t('Entity Not found'),
            $language ? $language->getName() : $document->getSourceLanguage(),
            $targetLanguage ? $targetLanguage->getName() : $document->getTargetLanguage(),
            Document::STATUSES[$document->getStatus()],
            $operations,
          ];
        }
      }
    }
    return [
      '#type' => 'page',
      'header' => ['#markup' => '<h1>' . $this->t('Dashboard') . '</h1>'],
      'content' => [
        ['#type' => 'status_messages'],
        ['#markup' => '<br>'],
        $table,
        ['#markup' => '<br>'],
        ['#type' => 'pager'],
      ],
    ];
  }

}
