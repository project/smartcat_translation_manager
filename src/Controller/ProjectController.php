<?php

namespace Drupal\smartcat_translation_manager\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller for work with project.
 */
class ProjectController extends ControllerBase {
  /**
   * Api service.
   *
   * @var \Drupal\smartcat_translation_manager\Api\Api
   */
  protected $api;

  /**
   * Repository project.
   *
   * @var \Drupal\smartcat_translation_manager\DB\Repository\ProjectRepository
   */
  protected $projectRepository;

  /**
   * Messanger.
   *
   * @var Messenger
   */
  protected $messenger;

  /**
   * Current HTTP request.
   *
   * @var Symfony\Component\HttpFoundation\Request
   */
  protected $request;
  /**
   * Temporary storage.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected $tempStore;

  /**
   * Extention state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Init dependencies.
   */
  public function __construct(ContainerInterface $container) {
    $this->api = $container->get('smartcat_translation_manager.api');
    $this->projectRepository = $container->get('smartcat_translation_manager.repository.project');
    $this->tempStore = $container->get('tempstore.private')->get('entity_translate_multiple_confirm');
    $this->messenger = $container->get('messenger');
    $this->request = $container->get('request_stack')->getCurrentRequest();
    $this->state = $container->get('state');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container);
  }

  /**
   * Method add translation project.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to settings page.
   *
   * @throws NotFoundHttpException
   *   Throw if not found entity by type and id.
   */
  public function add() {
    $type_id = $this->request->query->get('type_id');
    $entity_id = $this->request->query->get('entity_id');
    $lang = $this->request->query->get('lang');
    $lang = !is_array($lang) ? [$lang] : $lang;

    $this->state->set('smartcat_api_languages', $lang);

    $entity = $this->entityTypeManager()
      ->getStorage($type_id)
      ->load($entity_id);

    if (!$entity) {
      throw new NotFoundHttpException("Entity $type_id $entity_id not found");
    }

    $selection = [];

    $langcode = $entity->language()->getId();
    $selection[$entity->id()][$langcode] = $langcode;

    $this->tempStore->set($this->currentUser()->id() . ':' . $type_id, $selection);
    $previousUrl = $this->request->server->get('HTTP_REFERER');
    $base_url = $this->request::createFromGlobals()->getSchemeAndHttpHost();
    $destination = substr($previousUrl, strlen($base_url));

    return new RedirectResponse(
        Url::fromRoute('smartcat_translation_manager.settings_more',
            ['entity_type_id' => $type_id],
            ['query' => ['destination' => $destination]]
        )->toString()
    );
  }

}
