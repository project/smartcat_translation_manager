<?php

namespace Drupal\smartcat_translation_manager\DB\Entity;

/**
 * Entity for document.
 */
class Document {
  const STATUS_CREATED = "created";
  const STATUS_INPROGRESS = "inprogress";
  const STATUS_COMPLETED = "completed";
  const STATUS_CANCELED = "canceled";
  const STATUS_FAILED = "failed";
  const STATUS_DOWNLOADED = "downloaded";

  const STATUSES = [
    self::STATUS_CREATED => "Sent to Smartcat",
    self::STATUS_INPROGRESS => "Translating in Smartcat",
    self::STATUS_COMPLETED => "Translating in Smartcat",
    self::STATUS_DOWNLOADED => "Completed",
    self::STATUS_CANCELED => "Canceled",
    self::STATUS_FAILED => "Failed",
  ];

  /**
   * Incremental id.
   *
   * @var int
   */
  private $id;

  /**
   * Document name.
   *
   * @var string
   */
  private $name;

  /**
   * Entity id .
   *
   * @var int
   */
  private $entityId;

  /**
   * Entity type id.
   *
   * @var string
   */
  private $entityTypeId;

  /**
   * Source language code.
   *
   * @var string
   */
  private $sourceLanguage;

  /**
   * Target language code.
   *
   * @var string
   */
  private $targetLanguage;

  /**
   * Status of traslation.
   *
   * @var string
   */
  private $status;

  /**
   * Id for getting export.
   *
   * @var string
   */
  private $externalExportId = NULL;

  /**
   * Smartcat project id.
   *
   * @var string
   */
  private $externalProjectId;

  /**
   * Smartcat document id.
   *
   * @var string
   */
  private $externalDocumentId;

  /**
   * Get id.
   *
   * @return int
   *   Return id
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Set id.
   *
   * @param int $id
   *   Incremental id.
   *
   * @return Document
   *   Return self(Document)
   */
  public function setId($id) {
    $this->id = $id;

    return $this;
  }

  /**
   * Get document name.
   *
   * @return string
   *   Document name
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Set name.
   *
   * @param string $name
   *   Document name.
   *
   * @return Document
   *   Return self(Document)
   */
  public function setName($name) {
    $this->name = $name;

    return $this;
  }

  /**
   * Get entity id.
   *
   * @return int
   *   Entity id
   */
  public function getEntityId() {
    return $this->entityId;
  }

  /**
   * Set entity id.
   *
   * @param int $entityId
   *   Entity id.
   *
   * @return Document
   *   Return self(Document)
   */
  public function setEntityId($entityId) {
    $this->entityId = $entityId;

    return $this;
  }

  /**
   * Get entity type id.
   *
   * @return string
   *   Entity type id.
   */
  public function getEntityTypeId() {
    return $this->entityTypeId;
  }

  /**
   * Set entity type id.
   *
   * @param string $entityTypeId
   *   Entity type id.
   *
   * @return Document
   *   Return self(Document)
   */
  public function setEntityTypeId($entityTypeId) {
    $this->entityTypeId = $entityTypeId;

    return $this;
  }

  /**
   * Get smartcat project id.
   *
   * @return string
   *   Smartcat project id
   */
  public function getExternalProjectId() {
    return $this->externalProjectId;
  }

  /**
   * Set smartcat project id.
   *
   * @param string $externalProjectId
   *   Smartcat project id.
   *
   * @return Document
   *   Return self(Document)
   */
  public function setExternalProjectId($externalProjectId) {
    $this->externalProjectId = $externalProjectId;

    return $this;
  }

  /**
   * Get smartcat document id.
   *
   * @return string
   *   Smartcat document id
   */
  public function getExternalDocumentId() {
    return $this->externalDocumentId;
  }

  /**
   * Set smartcat document id.
   *
   * @param string $externalDocumentId
   *   Smartcat document id.
   *
   * @return Document
   *   Return self(Document)
   */
  public function setExternalDocumentId($externalDocumentId) {
    $this->externalDocumentId = $externalDocumentId;

    return $this;
  }

  /**
   * Get source lang cod.
   *
   * @return string
   *   Source lang code
   */
  public function getSourceLanguage() {
    return $this->sourceLanguage;
  }

  /**
   * Set source lang code.
   *
   * @param string $sourceLanguage
   *   Source lang code.
   *
   * @return Document
   *   Return self(Document)
   */
  public function setSourceLanguage($sourceLanguage) {
    $this->sourceLanguage = $sourceLanguage;

    return $this;
  }

  /**
   * Get target lang code.
   *
   * @return string
   *   Target lang code
   */
  public function getTargetLanguage() {
    return $this->targetLanguage;
  }

  /**
   * Set target lang code.
   *
   * @param string $targetLanguage
   *   Target lang code.
   *
   * @return Document
   *   Return self(Document)
   */
  public function setTargetLanguage($targetLanguage) {
    $this->targetLanguage = $targetLanguage;

    return $this;
  }

  /**
   * Get status.
   *
   * @return string
   *   Status translation document
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * Set status document.
   *
   * @param string $status
   *   Status document.
   *
   * @return Document
   *   Return self(Document)
   */
  public function setStatus($status) {
    $this->status = $status;

    return $this;
  }

  /**
   * Get export id.
   *
   * @return string
   *   Export id
   */
  public function getExternalExportId() {
    return $this->externalExportId;
  }

  /**
   * Set export id .
   *
   * @param string $externalExportId
   *   Export id.
   *
   * @return Document
   *   Return self(Document)
   */
  public function setExternalExportId($externalExportId) {
    $this->externalExportId = $externalExportId;

    return $this;
  }

}
