<?php

namespace Drupal\smartcat_translation_manager\DB\Entity;

/**
 * Entity for project.
 */
class Project {
  const STATUS_NEW = "new";
  const STATUS_CREATED = "created";
  const STATUS_INPROGRESS = "inprogress";
  const STATUS_COMPLETED = "completed";
  const STATUS_DOWNLOAD = "download";
  const STATUS_FINISHED = "finished";
  const STATUS_CANCELED = "canceled";
  const STATUS_ARCHIVED = "archived";
  const STATUS_FAILED = "failed";

  /**
   * Project id.
   *
   * @var int
   */
  private $id;

  /**
   * Project name.
   *
   * @var string
   */
  private $name;

  /**
   * Source language .
   *
   * @var string
   */
  private $sourceLanguage;

  /**
   * Target languages.
   *
   * @var array
   */
  private $targetLanguages;

  /**
   * Status.
   *
   * @var string
   */
  private $status;

  /**
   * Smartcat project id.
   *
   * @var string
   */
  private $externalProjectId = NULL;

  /**
   * Smartcat export id.
   *
   * @var string
   */
  private $externalExportId = NULL;

  /**
   * Get id .
   *
   * @return int
   *   Id
   */
  public function getId() {
    return $this->id;
  }

  /**
   * Set id.
   *
   * @param int $id
   *   Id.
   *
   * @return Project
   *   Return self(Project)
   */
  public function setId($id) {
    $this->id = $id;

    return $this;
  }

  /**
   * Get name.
   *
   * @return string
   *   Project name
   */
  public function getName() {
    return $this->name;
  }

  /**
   * Set name .
   *
   * @param string $name
   *   Project name.
   *
   * @return Project
   *   Return self(Project)
   */
  public function setName($name) {
    $this->name = $name;

    return $this;
  }

  /**
   * Get entity type id.
   *
   * @return string
   *   Entity type id
   */
  public function getEntityTypeId() {
    return $this->entityTypeId;
  }

  /**
   * Set entity type id.
   *
   * @param string $entityTypeId
   *   Entity type id.
   *
   * @return Project
   *   Return self(Project)
   */
  public function setEntityTypeId($entityTypeId) {
    $this->entityTypeId = $entityTypeId;

    return $this;
  }

  /**
   * Get source language .
   *
   * @return string
   *   Source lang
   */
  public function getSourceLanguage() {
    return $this->sourceLanguage;
  }

  /**
   * Set source lang.
   *
   * @param string $sourceLanguage
   *   Source lang.
   *
   * @return Project
   *   Return self(Project)
   */
  public function setSourceLanguage($sourceLanguage) {
    $this->sourceLanguage = $sourceLanguage;

    return $this;
  }

  /**
   * Get target langs.
   *
   * @return array
   *   Target langs
   */
  public function getTargetLanguages() {
    return $this->targetLanguages;
  }

  /**
   * Set target langs.
   *
   * @param array $targetLanguages
   *   Target langs.
   *
   * @return Project
   *   Return self(Project)
   */
  public function setTargetLanguages(array $targetLanguages) {
    $this->targetLanguages = $targetLanguages;

    return $this;
  }

  /**
   * Get Status .
   *
   * @return string
   *   Status
   */
  public function getStatus() {
    return $this->status;
  }

  /**
   * Set status.
   *
   * @param string $status
   *   Status.
   *
   * @return Project
   *   Return self(Project)
   */
  public function setStatus($status) {
    $this->status = $status;

    return $this;
  }

  /**
   * Get smartcat project id.
   *
   * @return string
   *   Smartcat project id
   */
  public function getExternalProjectId() {
    return $this->externalProjectId;
  }

  /**
   * Set smartcat project id.
   *
   * @param string $externalProjectId
   *   Smartcat project id.
   *
   * @return Project
   *   Return self(Project)
   */
  public function setExternalProjectId($externalProjectId) {
    $this->externalProjectId = $externalProjectId;

    return $this;
  }

  /**
   * Get smartcat export id.
   *
   * @return string
   *   Smartcat export id
   */
  public function getExternalExportId() {
    return $this->externalExportId;
  }

  /**
   * Set smartcat export id.
   *
   * @param string $externalExportId
   *   Smartcat export id.
   *
   * @return Project
   *   Return self(Project)
   */
  public function setExternalExportId($externalExportId) {
    $this->externalExportId = $externalExportId;

    return $this;
  }

}
