<?php

namespace Drupal\smartcat_translation_manager\DB\Repository;

/**
 * Abstract table repository.
 */
abstract class RepositoryAbstract implements RepositoryInterface {

  const TABLE_PREFIX = 'smartcat_translation_manager_';

  protected $connection;

  /**
   * Init dependencies.
   */
  public function __construct() {
    $this->connection = \Drupal::database();
  }

  private $persists = [];

  /**
   * {@inheritdoc}
   */
  public function persist($o) {
    $this->persists[] = $o;
  }

  /**
   * Implementation flush for curent entity.
   *
   * @param array $persists
   *   Array entities.
   */
  abstract protected function doFlush(array $persists);

  /**
   * {@inheritdoc}
   */
  public function flush() {
    $this->doFlush($this->persists);
    $this->persists = [];
  }

  /**
   * Map row data to entity.
   *
   * @param object $row
   *   Row data.
   *
   * @return object
   *   current entity
   */
  abstract protected function toEntity($row);

  /**
   * Map array rows to array entities.
   *
   * @param array $rows
   *   Rows from database.
   */
  protected function prepareResult(array $rows) {
    $result = [];
    foreach ($rows as $row) {
      $result[] = $this->toEntity($row);
    }

    return $result;
  }

  /**
   * Search one row by criterias.
   *
   * @param array $criterias
   *   Search criterias.
   *
   * @return array|null
   *   Return result search query
   */
  public function getOneBy(array $criterias) {
    $table_name = $this->getTableName();
    $query = $this->connection->select($table_name, 's')
      ->fields('s');

    foreach ($criterias as $key => $value) {
      $query->condition($key, $value);
    }

    $row = $query->execute()->fetchObject();
    return $row ? $this->toEntity($row) : NULL;
  }

  /**
   * Search one or more rows by criterias.
   *
   * @param array $criterias
   *   Search criterias.
   * @param int $offset
   *   Search ofset.
   * @param int $limit
   *   Search limit.
   * @param array $order
   *   Order result.
   *
   * @return array
   *   Return result search query
   */
  public function getBy(array $criterias = [], int $offset = 0, int $limit = 10, array $order = []) {
    $table_name = $this->getTableName();
    $query = $this->connection->select($table_name, 's')
      ->fields('s');

    if (!empty($criterias)) {
      foreach ($criterias as $key => $value) {
        if (is_array($value)) {
          $query->condition($key, $value[0], $value[1]);
          continue;
        }
        $query->condition($key, $value);
      }
    }
    if (!empty($order)) {
      foreach ($order as $field => $direction) {
        $query->orderBy($field, $direction);
      }
    }

    $query->range($offset, $limit);

    $result = $query->execute();
    $entities = [];
    foreach ($result as $record) {
      $entities[] = $this->toEntity($record);
    }
    return $entities;
  }

  /**
   * Count result by criterias.
   *
   * @param array $criterias
   *   Query criterias.
   *
   * @return int
   *   Count result
   */
  public function count(array $criterias = []) {
    $table_name = $this->getTableName();
    $query = $this->connection->select($table_name, 's')
      ->fields('s');
    if (!empty($criterias)) {
      foreach ($criterias as $key => $value) {
        if (is_array($value)) {
          $query->condition($key, $value[0], $value[1]);
          continue;
        }
        $query->condition($key, $value);
      }
    }
    return $query->countQuery()->execute()->fetchField();
  }

  /**
   * Mass row update in DB.
   *
   * @param array $data
   *   Fields need update.
   * @param array $criterias
   *   Criterias for update.
   *
   * @return mixed
   *   Return result executed query update
   */
  public function bulkUpdate(array $data, array $criterias) {
    $table_name = $this->getTableName();
    $query = $this->connection->update($table_name)
      ->fields($data);

    if (empty($criterias)) {
      return FALSE;
    }

    foreach ($criterias as $key => $value) {
      if (is_array($value)) {
        $query->condition($key, $value[0], $value[1]);
        continue;
      }
      $query->condition($key, $value);
    }

    try {
      return $query->execute();
    }
    catch (\Exception $e) {
      return FALSE;
    }
  }

}
