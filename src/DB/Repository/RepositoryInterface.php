<?php

namespace Drupal\smartcat_translation_manager\DB\Repository;

/**
 * Interface for repository.
 */
interface RepositoryInterface {

  /**
   * Return current table name.
   *
   * @return string
   *   Table name
   */
  public function getTableName();

  /**
   * Return current table schema.
   *
   * @return array
   *   Schema for table.
   */
  public function getSchema();

  /**
   * Set list entities to persists.
   *
   * @param array $o
   *   Array of entitties.
   */
  public function persist(array $o);

  /**
   * Save persist entities to database.
   */
  public function flush();

}
