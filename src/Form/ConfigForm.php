<?php

namespace Drupal\smartcat_translation_manager\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\State\StateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use SmartCat\Client\SmartCat;

/**
 * Form for common configuration.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * Extention state.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;
  /**
   * Messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\State\StateInterface $state
   *   Provides the state system using a key value store.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    StateInterface $state,
    MessengerInterface $messenger
  ) {
    $this->state = $state;
    $this->messenger = $messenger;
    parent::__construct($config_factory);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('state'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'smartcat_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['api_server'] = [
      '#title' => $this->t('Server', [], ['context' => 'smartcat_translation_manager']),
      '#type' => 'select',
      '#options' => [
        SmartCat::SC_EUROPE => $this->t('Europe', [], ['context' => 'smartcat_translation_manager']),
        SmartCat::SC_USA => $this->t('USA', [], ['context' => 'smartcat_translation_manager']),
        SmartCat::SC_ASIA => $this->t('Asia', [], ['context' => 'smartcat_translation_manager']),
      ],
    ];

    $form['api_login'] = [
      '#title' => $this->t('Smartcat account ID', [], ['context' => 'smartcat_translation_manager']),
      '#type' => 'textfield',
      '#default_value' => $this->state->get('smartcat_api_login', ''),
      '#required' => TRUE,
    ];

    $lenpass = strlen($this->state->get('smartcat_api_password', ''));
    $form['api_password'] = [
      '#title' => $this->t('API key', [], ['context' => 'smartcat_translation_manager']),
      '#type' => 'password',
      '#placeholder' => $lenpass > 0 ? str_repeat('•', $lenpass) : '',
      '#required' => TRUE,
    ];

    $accountName = $this->state->get('smartcat_account_name', '');
    if (!empty($accountName)) {
      $form['info'] = [
        '#title' => "You connected to account: $accountName",
        '#type' => 'item',
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $login = $form_state->getValues()['api_login'];
    $password = $form_state->getValues()['api_password'];
    $server = $form_state->getValues()['api_server'];
    try {
      $api = new SmartCat($login, $password, $server);
      $api->getAccountManager()->accountGetAccountInfo();
    }
    catch (\Exception $e) {
      $this->messenger->addError($this->t('Invalid Smartcat account ID or API key', [], ['context' => 'smartcat_translation_manager']));
      $form_state->setError($form['api_login']);
      $form_state->setError($form['api_password']);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $formValues = $form_state->getValues();

    $this->state->set('smartcat_api_login', $formValues['api_login']);
    $this->state->set('smartcat_api_password', $formValues['api_password']);
    $this->state->set('smartcat_api_server', $formValues['api_server']);

    $api = new SmartCat($formValues['api_login'], $formValues['api_password'], $formValues['api_server']);
    try {
      $account_info = $api->getAccountManager()->accountGetAccountInfo();
    }
    catch (\Exception $e) {
      $this->messenger->addError($this->t('Invalid Smartcat account ID or API key', [], ['context' => 'smartcat_translation_manager']));
    }

    // сохраняем account_name.
    if ($account_info && $account_info->getName()) {
      $this->state->set('smartcat_account_name', $account_info->getName());
    }
    $this->messenger->addMessage($this->t('The configuration options have been saved.', [], ['context' => 'smartcat_translation_manager']));
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {

  }

}
