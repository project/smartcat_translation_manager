<?php

namespace Drupal\smartcat_translation_manager\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\smartcat_translation_manager\Api\Api;
use Drupal\smartcat_translation_manager\DB\Entity\Document;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Form for setting translation configurations.
 */
class RefreshConfirm extends ConfirmFormBase {
  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Document repository.
   *
   * @var \Drupal\smartcat_translation_manager\DB\Entity\DocumentRepository
   */
  protected $documentRepository;

  /**
   * Current HTTP request.
   *
   * @var Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Messenger.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * Init dependencies.
   */
  public function __construct() {
    $this->entityTypeManager = \Drupal::entityTypeManager();
    $this->documentRepository = \Drupal::service('smartcat_translation_manager.repository.document');
    $this->request = \Drupal::service('request_stack')->getCurrentRequest();
    $this->messenger = \Drupal::service('messenger');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'smartcat_config_more_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to refresh translation?');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $entity_type_id = NULL) {
    $api = new Api(\Drupal::state());

    try {
      $api->getAccount();
    }
    catch (\Exception $e) {
      \Drupal::messenger()->addError(t('Invalid Smartcat account ID or API key. Please check <a href=":url">your credentials</a>.', [
        ':url' => Url::fromRoute('smartcat_translation_manager.settings')->toString(),
      ], ['context' => 'smartcat_translation_manager']));
      return new RedirectResponse($this->getCancelUrl()
        ->setAbsolute()
        ->toString());
    }

    $document_id = $this->request->query->get('document_id', FALSE);
    $destination = $this->request->query->get('destination', \Drupal::request()->server->get('HTTP_REFERER'));

    if ($document_id === FALSE) {
      return new RedirectResponse(Url::fromRoute('smartcat_translation_manager.document')->toString());
    }

    $form['document_id'] = [
      '#type' => 'hidden',
      '#default_value' => $document_id,
    ];

    $form['destination'] = [
      '#type' => 'hidden',
      '#default_value' => $destination,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    try {
      \Drupal::getContainer()->get('smartcat_translation_manager.api')->getAccount();
    }
    catch (\Exception $e) {
      \Drupal::getContainer()->get('messenger')->addError(t('Invalid Smartcat account ID or API key. Please check <a href=":url">your credentials</a>.', [
        ':url' => Url::fromRoute('smartcat_translation_manager.settings')->toString(),
      ], ['context' => 'smartcat_translation_manager']));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $formValues = $form_state->getValues();

    $id = $formValues['document_id'];

    $document = $this->documentRepository->getOneBy(['id' => $id]);
    if ($document->getStatus() === Document::STATUS_DOWNLOADED) {
      $document->setStatus(Document::STATUS_INPROGRESS);
      $document->setExternalExportId(NULL);
      $this->documentRepository->update($document);
      $this->messenger->addMessage($this->t('Your request for translation updates was successfully submitted.', [], ['context' => 'smartcat_translation_manager']));
    }
    $prev = $formValues['destination'];
    if ($prev) {
      return new RedirectResponse($prev);
    }
    return new RedirectResponse(Url::fromRoute('smartcat_translation_manager.document')->toString());
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    if ($this->entityType->hasLinkTemplate('collection')) {
      return new Url('entity.' . $this->entityTypeId . '.collection');
    }
    else {
      if ($prev = \Drupal::request()->query->get('destination', FALSE)) {
        return Url::fromUri("internal:$prev");
      }
      return new Url('<front>');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {

  }

}
