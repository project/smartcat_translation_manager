<?php

namespace Drupal\smartcat_translation_manager\Helper;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\smartcat_translation_manager\DB\Entity\Project;
use Drupal\smartcat_translation_manager\DB\Entity\Document;

/**
 * Helper for API.
 */
class ApiHelper {

  /**
   * Filter special chars.
   */
  public static function filterChars($s) {
    return mb_substr(str_replace(['*', '|', '\\', ':', '"', '<', '>', '?', '/'], '_', $s), 0, 94);
  }

  /**
   * Generate project name a link.
   *
   * @param \Drupal\smartcat_translation_manager\DB\Entity\Project $project
   *   Entity project.
   *
   * @return \Drupal\Core\Link
   *   Link to project
   */
  public static function getProjectName(Project $project) {
    $name = $project->getName();
    if ($project->getExternalProjectId()) {
      $projectUrl = self::getProjectUrl($project);
      $name = Link::fromTextAndUrl($project->getName(), $projectUrl)->toString();
    }
    return $name;
  }

  /**
   * Get url to smartcat project.
   *
   * @param \Drupal\smartcat_translation_manager\DB\Entity\Project $project
   *   Entitty project.
   *
   * @return \Drupal\Core\Url
   *   Url to project
   */
  public static function getProjectUrl(Project $project) {
    $state = \Drupal::state();
    return Url::fromUri("https://{$state->get('smartcat_api_server')}/projects/{$project->getExternalProjectId()}");
  }

  /**
   * Get url to smartcat project by document.
   *
   * @param \Drupal\smartcat_translation_manager\DB\Entity\Document $document
   *   Entity documnet.
   *
   * @return \Drupal\Core\Url
   *   Url to project
   */
  public static function getProjectUrlBydocument(Document $document) {
    $state = \Drupal::state();
    return Url::fromUri("https://{$state->get('smartcat_api_server')}/projects/{$document->getExternalProjectId()}");
  }

  /**
   * Get url to smartcat document.
   *
   * @param \Drupal\smartcat_translation_manager\DB\Entity\Document $document
   *   Entity document.
   *
   * @return \Drupal\Core\Url
   *   Url to smartcat document
   */
  public static function getDocumentUrl(Document $document) {
    return Url::fromUri(self::getDocumentLink($document->getExternalDocumentId()));
  }

  /**
   * Generate document link.
   *
   * @param string $document_id
   *   Smartcat document id.
   *
   * @return string
   *   Url to document
   */
  public static function getDocumentLink($document_id) {
    $ids = explode('_', $document_id);
    $state = \Drupal::state();
    return "https://{$state->get('smartcat_api_server')}/editor?DocumentId={$ids[0]}&LanguageId={$ids[1]}";
  }

}
