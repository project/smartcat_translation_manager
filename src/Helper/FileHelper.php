<?php

namespace Drupal\smartcat_translation_manager\Helper;

use Drupal\Core\Entity\EntityInterface;

/**
 * Map entity to html and back.
 */
class FileHelper {
  const FIELD_TAG = '<field id="%s">%s</field>';

  /**
   * Init dependencies.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Drupal entity.
   */
  public function __construct(EntityInterface $entity) {
    $this->entity = $entity;
  }

  /**
   * Create file by entity.
   *
   * @param array $useFields
   *   Use field, example ['title','body','comment'].
   *
   * @return string
   *   Path to file
   */
  public function createFileByEntity(array $useFields = []) {
    $file = \file_save_data($this->generateHtmlMarkup($useFields));

    if (!$file) {
      return '';
    }

    return \Drupal::service('file_system')->realpath($file->get('uri')->value);
  }

  /**
   * Method generate markup by entity.
   *
   * @param array $useFields
   *   Fields names.
   *
   * @return string
   *   Html markup
   */
  protected function generateHtmlMarkup(array $useFields) {
    $data = [];
    $fields = $this->entity->getFieldDefinitions();

    foreach ($fields as $field) {
      if (!$field->isTranslatable()) {
        continue;
      }
      if (!empty($useFields) && !in_array($field->getName(), $useFields)) {
        continue;
      }
      $data[] = sprintf(self::FIELD_TAG, $field->getName(), $this->entity->get($field->getName())->value);
      if ($field->getName() === 'body') {
        $data[] = sprintf(self::FIELD_TAG, $field->getName() . '_summary', $this->entity->get($field->getName())->summary);
      }
    }

    return '<html><head></head><body>' . implode('', $data) . '</body></html>';
  }

  /**
   * Create or update entity translation.
   *
   * @param string $content
   *   Content(HTML markup)
   * @param string $langcode
   *   Language code.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Drupal entity
   *
   * @throws \Exception
   *   Throw if setting field throw error.
   */
  public function markupToEntityTranslation($content, $langcode) {
    $fieldPattern = str_replace('/', '\/', sprintf(self::FIELD_TAG, '(.+?)', '(.*?)'));

    $matches = [];

    preg_match_all('/' . $fieldPattern . '/is', $content, $matches);

    if ($this->hasTranslation($langcode)) {
      $entity_translation = $this->entity->getTranslation($langcode);
    }
    else {
      $entity_translation = $this->entity->addTranslation($langcode, $this->entity->toArray());
    }

    foreach ($matches[1] as $i => $field) {
      $value = $matches[2][$i];

      if ($field === 'body' || $field === 'comment_body') {
        $value = [
          'value' => $value,
          'format' => $entity_translation->get($field)->format,
        ];
      }
      else {
        $value = $this->specialcharsDecode($value);
      }

      if ($field === 'body_summary') {
        $field = 'body';
        $value = [
          'value' => $entity_translation->get($field)->value,
          'summary' => $value,
          'format' => $entity_translation->get($field)->format,
        ];
      }

      try {
        $entity_translation->set($field, $value);
      }
      catch (\Exception $e) {
        throw new \Exception("field = $field, value = $value, error = {$e->getMessage()}");
      }
    }

    return $entity_translation;
  }

  /**
   * Decode unicode special char.
   *
   * @param string $str
   *   Raw string.
   *
   * @return string
   *   Decoded string
   */
  public function specialcharsDecode($str) {
    $str = html_entity_decode($str);
    $str = str_replace('&#39;', "'", $str);
    return $str;
  }

  /**
   * Check existing translation.
   *
   * @param string $langcode
   *   Language code.
   *
   * @return bool
   *   Existing translation
   */
  public function hasTranslation($langcode) {
    $existing_translation = \Drupal::service('entity.repository')->getTranslationFromContext($this->entity, $langcode);
    return ($existing_translation->langcode->value === $langcode) ? TRUE : FALSE;
  }

}
