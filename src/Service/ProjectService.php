<?php

namespace Drupal\smartcat_translation_manager\Service;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\smartcat_translation_manager\Api\Api;
use Drupal\smartcat_translation_manager\DB\Entity\Document;
use Drupal\smartcat_translation_manager\DB\Entity\Project;
use Drupal\smartcat_translation_manager\DB\Repository\DocumentRepository;
use Drupal\smartcat_translation_manager\DB\Repository\ProjectRepository;
use Drupal\smartcat_translation_manager\Helper\FileHelper;
use Drupal\smartcat_translation_manager\Helper\ApiHelper;
use Drupal\smartcat_translation_manager\Helper\LanguageCodeConverter;

/**
 * Service for work with project .
 */
class ProjectService {
  /**
   * Faced smartcat API.
   *
   * @var \Drupal\smartcat_translation_manager\Api\Api
   */
  protected $api;

  /**
   * Project repository.
   *
   * @var \Drupal\smartcat_translation_manager\DB\Entity\ProjectRepository
   */
  protected $projectRepository;

  /**
   * Document repository.
   *
   * @var \Drupal\smartcat_translation_manager\DB\Entity\DocumentRepository
   */
  protected $documentRepository;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Project entity.
   *
   * @var \Drupal\smartcat_translation_manager\DB\Entity\Project
   */
  protected $project;

  /**
   * List documents.
   *
   * @var array
   */
  protected $documents = [];

  /**
   * Init dependencies.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entityManager
   *   Entity manager.
   * @param \Drupal\smartcat_translation_manager\Api\Api $api
   *   Faced API.
   * @param \Drupal\smartcat_translation_manager\DB\Entity\ProjectRepository $projectRepository
   *   Project repository.
   * @param \Drupal\smartcat_translation_manager\DB\Entity\DocumentRepository $documentRepository
   *   Documnet repository.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger
   *   Logger factory.
   */
  public function __construct(
    EntityManagerInterface $entityManager,
    Api $api,
    ProjectRepository $projectRepository,
    DocumentRepository $documentRepository,
    LoggerChannelFactory $logger
  ) {
    $this->entityManager = $entityManager;
    $this->api = $api;
    $this->documentRepository = $documentRepository;
    $this->projectRepository = $projectRepository;
    $this->logger = $logger->get('smartcat_translation_manager_project');
  }

  /**
   * Create project.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity.
   * @param array $translateTo
   *   List lang codes.
   */
  public function createProject(EntityInterface $entity, array $translateTo = NULL) {
    $this->project = (new Project())
      ->setName($entity->label())
      ->setEntityTypeId($entity->getEntityTypeId())
      ->setSourceLanguage($entity->language()->getId())
      ->setTargetLanguages($translateTo)
      ->setStatus(Project::STATUS_NEW);
  }

  /**
   * Send project to smartcat.
   *
   * @return \Drupal\smartcat_translation_manager\DB\Entity\Project
   *   Entity project
   */
  public function sendProject() {
    if (empty($this->project)) {
      return NULL;
    }
    $project = $this->project;
    $scProject = $this->api->createProject($project);

    $project->setExternalProjectId($scProject->getId());
    $project->setName($scProject->getName());

    $project->setId($this->projectRepository->add($project));
    $this->project = NULL;
    return $project;
  }

  /**
   * Send documets to smartcat.
   *
   * @param \Drupal\smartcat_translation_manager\DB\Entity\Project $project
   *   Entity project.
   *
   * @return \SmartCat\Client\Model\DocumentModel[]
   *   Array document model
   */
  public function sendDocuments(Project $project) {
    $documents = $this->addDocuments(array_values($this->documents), $project->getExternalProjectId());
    if (empty($documents)) {
      return FALSE;
    }

    foreach ($documents as $scDocument) {
      $matches = [];
      preg_match('/-(\d+)$/', $scDocument->getName(), $matches);
      $this->documentRepository->add(
        (new Document())
          ->setName($scDocument->getName())
          ->setEntityId($matches[1])
          ->setEntityTypeId($project->getEntityTypeId())
          ->setSourceLanguage(LanguageCodeConverter::convertSmartcatToDrupal($scDocument->getSourceLanguage()))
          ->setTargetLanguage(LanguageCodeConverter::convertSmartcatToDrupal($scDocument->getTargetLanguage()))
          ->setStatus($scDocument->getStatus())
          ->setExternalProjectId($project->getExternalProjectId())
          ->setExternalDocumentId($scDocument->getId())
      );
    }
    $this->documents = [];
    return $documents;
  }

  /**
   * Send project with documents.
   */
  public function sendProjectWithDocuments() {

    $project = $this->sendProject();
    $this->sendDocuments($project);

  }

  /**
   * Add entity to translate.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Drupal entity.
   * @param array $translateTo
   *   List language codes.
   *
   * @return \Drupal\smartcat_translation_manager\Service\ProjectService
   *   Return self(ProjectService)
   */
  public function addEntityToTranslete(EntityInterface $entity, array $translateTo) {
    if (empty($this->project)) {
      $this->createProject($entity, $translateTo);
    }
    else {
      $this->project->setName("{$this->project->getName()}, {$entity->label()}");
    }
    $this->documents[$entity->id()] = $this->createDocument($entity);
    return $this;
  }

  /**
   * Add documents to smartcat project.
   *
   * @param \SmartCat\Client\Model\DocumentModel[] $documents
   *   List documents.
   * @param string $externalProjectId
   *   Smartcat project id.
   *
   * @return \SmartCat\Client\Model\DocumentModel[]
   *   List documents
   */
  protected function addDocuments(array $documents, $externalProjectId) {
    try {
      $documents = $this->api->getProjectManager()->projectAddDocument([
        'documentModel' => $documents,
        'projectId' => $externalProjectId,
      ]);
    }
    catch (\Exception $e) {
      $this->logger->info("{$e->getResponse()->getStatusCode()}, {$e->getMessage()}, {$e->getResponse()->getBody()->getContents()}");
      return [];
    }

    return $documents;
  }

  /**
   * Create document.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Drupal entity.
   *
   * @return \SmartCat\Client\Model\DocumentModel
   *   Smartcat document model
   */
  protected function createDocument(EntityInterface $entity) {
    $fieldDefinitions = $this->entityManager->getFieldDefinitions($entity->getEntityTypeId(), $entity->bundle());
    $translatable = [];

    foreach ($fieldDefinitions as $fieldName => $fieldDefinition) {
      if (($fieldDefinition->isComputed() || $this->isFieldTranslatabilityConfigurable($entity, $fieldName))) {
        array_push($translatable, $fieldName);
      }
    }

    if (empty($translatable)) {
      $translatable = ['title', 'body', 'comment'];
    }

    $file = (new FileHelper($entity))->createFileByEntity($translatable);
    $fileName = ApiHelper::filterChars(\sprintf('%s-%d.html', $entity->label(), $entity->id()));
    return $this->api->project->createDocumentFromFile($file, $fileName);
  }

  /**
   * Check translatability configurable field.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Drupal document.
   * @param string $field_name
   *   Field name.
   */
  protected function isFieldTranslatabilityConfigurable(EntityInterface $entity, $field_name) {
    $entity_type = $this->entityManager->getDefinition($entity->getEntityTypeId());
    $storage_definitions = $this->entityManager->getFieldStorageDefinitions($entity->getEntityTypeId());
    $fields = [
      $entity_type->getKey('langcode'),
      $entity_type->getKey('default_langcode'),
      'revision_translation_affected',
    ];

    return !empty($storage_definitions[$field_name]) &&
            $storage_definitions[$field_name]->isTranslatable() &&
            $storage_definitions[$field_name]->getProvider() != 'content_translation' &&
            !in_array($field_name, $fields);
  }

}
